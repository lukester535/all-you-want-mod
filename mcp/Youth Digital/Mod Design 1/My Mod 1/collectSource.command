#!/bin/bash

DIR_NAME=`dirname "$0"`

cd "${DIR_NAME}"
echo $DIR_NAME
rm -r My\ Mod\ Source
rm -r mcp/src/minecraft/mytroublemod
rm -r mcp/src/minecraft/assets/troublemod
mkdir My\ Mod\ Source
cp -r mcp/src/minecraft/assets My\ Mod\ Source/
cp -r mcp/src/minecraft/mymod My\ Mod\ Source/
cp -r mcp/src/minecraft/TEMPLATES My\ Mod\ Source/

osascript -e 'tell application "Terminal" to quit' & exit