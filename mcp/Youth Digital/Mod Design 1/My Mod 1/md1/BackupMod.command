#!/bin/bash


DIR_RUN=`dirname "$0"`

cd "${DIR_RUN}"

bck_number=`ls "ModBackups" | awk '{ if ( $1 == "Bck" ) { print $0; } } ' | wc -l`
#echo $mod_number
a_number=`echo "${bck_number}+1" | bc`
#echo $a_number
mkdir "ModBackups/Bck ${a_number}"

cp -r ../mcp/src/minecraft/assets "ModBackups/Bck ${a_number}/"
cp -r ../mcp/src/minecraft/mymod "ModBackups/Bck ${a_number}/"

osascript -e 'tell application "Terminal" to quit' & exit