#!/bin/bash


DIR_RUN=`dirname "$0"`

cd "${DIR_RUN}"

rm -r ../mcp/src/minecraft/mytroublemod/

mkdir ModTmp
cp -r ../mcp/src/minecraft/assets ModTmp/
cp -r ../mcp/src/minecraft/mymod ModTmp/

cp -r SrcBackup/ ../mcp/src/minecraft/
cp -r StarterFiles/ ../mcp/

cp -r ModTmp/ ../mcp/src/minecraft/

rm -r ModTmp

osascript -e 'tell application "Terminal" to quit' & exit