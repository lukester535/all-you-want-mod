#!/bin/bash

DIR_RUN=`dirname "$0"`
cd "${DIR_RUN}"
rm -r My\ Mod\ Export
rm -r mcp/src/minecraft/mytroublemod
rm -r mcp/src/assets/troublemod

cd mcp
./recompile.sh
./reobfuscate.sh

cd ..

mkdir My\ Mod\ Export
mkdir My\ Mod\ Export/assets
cp -r mcp/src/minecraft/assets/mymod My\ Mod\ Export/assets/
cp -r mcp/reobf/minecraft/mymod My\ Mod\ Export/

osascript -e 'tell application "Terminal" to quit' & exit