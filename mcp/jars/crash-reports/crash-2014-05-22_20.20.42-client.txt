---- Minecraft Crash Report ----
// On the bright side, I bought you a teddy bear!

Time: 5/22/14 8:20 PM
Description: There was a severe problem during mod loading that has caused the game to fail

cpw.mods.fml.common.LoaderException
	at cpw.mods.fml.common.registry.ItemData.setName(ItemData.java:161)
	at cpw.mods.fml.common.registry.GameData.setName(GameData.java:257)
	at cpw.mods.fml.common.registry.GameRegistry.registerItem(GameRegistry.java:151)
	at cpw.mods.fml.common.registry.GameRegistry.registerItem(GameRegistry.java:139)
	at mymod.Main.preInit(Main.java:152)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:597)
	at cpw.mods.fml.common.FMLModContainer.handleModStateEvent(FMLModContainer.java:545)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:597)
	at com.google.common.eventbus.EventHandler.handleEvent(EventHandler.java:74)
	at com.google.common.eventbus.SynchronizedEventHandler.handleEvent(SynchronizedEventHandler.java:45)
	at com.google.common.eventbus.EventBus.dispatch(EventBus.java:313)
	at com.google.common.eventbus.EventBus.dispatchQueuedEvents(EventBus.java:296)
	at com.google.common.eventbus.EventBus.post(EventBus.java:267)
	at cpw.mods.fml.common.LoadController.sendEventToModContainer(LoadController.java:201)
	at cpw.mods.fml.common.LoadController.propogateStateMessage(LoadController.java:181)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:597)
	at com.google.common.eventbus.EventHandler.handleEvent(EventHandler.java:74)
	at com.google.common.eventbus.SynchronizedEventHandler.handleEvent(SynchronizedEventHandler.java:45)
	at com.google.common.eventbus.EventBus.dispatch(EventBus.java:313)
	at com.google.common.eventbus.EventBus.dispatchQueuedEvents(EventBus.java:296)
	at com.google.common.eventbus.EventBus.post(EventBus.java:267)
	at cpw.mods.fml.common.LoadController.distributeStateMessage(LoadController.java:112)
	at cpw.mods.fml.common.Loader.loadMods(Loader.java:522)
	at cpw.mods.fml.client.FMLClientHandler.beginMinecraftLoading(FMLClientHandler.java:183)
	at net.minecraft.client.Minecraft.startGame(Minecraft.java:473)
	at net.minecraft.client.Minecraft.run(Minecraft.java:808)
	at net.minecraft.client.main.Main.main(Main.java:93)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:597)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:131)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:27)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.6.4
	Operating System: Mac OS X (x86_64) version 10.9.2
	Java Version: 1.6.0_65, Apple Inc.
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Apple Inc.
	Memory: 975631368 bytes (930 MB) / 1060372480 bytes (1011 MB) up to 1060372480 bytes (1011 MB)
	JVM Flags: 3 total; -Xincgc -Xmx1024M -Xms1024M
	AABB Pool Size: 0 (0 bytes; 0 MB) allocated, 0 (0 bytes; 0 MB) used
	Suspicious classes: FML and Forge are installed
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP v8.11 FML v6.4.45.953 Minecraft Forge 9.11.1.953 4 mods loaded, 4 mods active
	mcp{8.09} [Minecraft Coder Pack] (minecraft.jar) Unloaded->Constructed->Pre-initialized
	FML{6.4.45.953} [Forge Mod Loader] (bin) Unloaded->Constructed->Pre-initialized
	Forge{9.11.1.953} [Minecraft Forge] (bin) Unloaded->Constructed->Pre-initialized
	mymod{1.0} [StuffCraft] (bin) Unloaded->Constructed->Errored