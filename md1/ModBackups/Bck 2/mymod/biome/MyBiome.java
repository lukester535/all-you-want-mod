package mymod.biome;

import mymod.Main;
import net.minecraft.block.Block;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.passive.EntityMooshroom;    
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.SpawnListEntry;

public class MyBiome extends BiomeGenBase
{
    public MyBiome(int par1)
    {
        super(par1);
        
        this.setBiomeName("HOTDOGISH CAKISH BIOME OF HOTDOCAKE AWESOMNESS");
        
        this.topBlock = (byte)Main.MyBlock_1.blockID;
        this.fillerBlock = (byte)Block.cake.blockID;
        

        this.spawnableCreatureList.add(new SpawnListEntry(EntityWolf.class, 999999999, 4, 7));
        this.spawnableCreatureList.add(new SpawnListEntry(EntitySkeleton.class, 30, 4, 7));
        
        
        this.setMinMaxHeight(0.1F, 0.1F);
        this.setTemperatureRainfall(1.5F, 0.2F);
   
    }
}