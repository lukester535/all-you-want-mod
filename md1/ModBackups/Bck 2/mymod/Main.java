package mymod;

import java.awt.Color;

import mymod.armor.MyArmor;
import mymod.biome.MyBiome;
import mymod.blocks.MyBlock;
import mymod.entity.creeper.MyEntityCreeper;
import mymod.entity.creeper.MyRenderCreeper;
import mymod.entity.minion.MyEntityMinion;
import mymod.entity.minion.MyModelMinion;
import mymod.entity.minion.MyRenderMinion;
import mymod.entity.zombie.MyEntityZombie;
import mymod.entity.zombie.MyRenderZombie;
import mymod.items.MyFood;
import mymod.items.MyItem;
import mymod.items.MyPickaxe;
import mymod.items.MySword;
import mymod.proxies.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityEggInfo;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.src.ModLoader;
import net.minecraft.stats.StatBase;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;


/* 	MOD INFO */
	@Mod( modid = "mymod", name = "ALL-U-WANT mod", version = "1.0")
	@NetworkMod(clientSideRequired=true, serverSideRequired=false)	


public class Main {

/*	PROXY INFO */
	@SidedProxy(clientSide = "mymod.proxies.ClientProxy", serverSide = "mymod.proxies.CommonProxy")
	public static CommonProxy proxy;
		
	
/**	
 * DECLARATION SECTION 
 * *********************************************************** */

//  DECLARE THE SWORD 
    public static Item MySword_1;

//  DECLARE THE PICKAXE 
    public static Item MyPickaxe_1;
    
//  DECLARE THE POOPAXE 
    public static Item MyPickaxe_2;
    
    
//  DECLARE NEW TOOL MATERIAL
    														/** Harvest Level, Max Uses, Efficiency, Damage Enchantability (f) */
    public static EnumToolMaterial MyToolMaterial = EnumHelper.addToolMaterial("!", 9, 1000000, 99.0F, 0.0F, 40);
    
//  DECLARE NEW TOOL MATERIAL
	/** Harvest Level, Max Uses, Efficiency, Damage Enchantability (f) */
public static EnumToolMaterial MyToolMaterial2 = EnumHelper.addToolMaterial("!!", 2000099999, 999999999, 99999.99F, 999999.9999F, 999999);
    
//  DECLARE NEW TOOL MATERIAL
							/** Harvest Level, Max Uses, Efficiency, Damage Enchantability (f) */
public static EnumToolMaterial LOL = EnumHelper.addToolMaterial("!", 3, 1000000, 1.0F, 10000.0F, 200);

//  DECLARE THE ITEM
public static Item MyItem_1;

//DECLARE THE ITEM2
public static Item MyItem_2;

//DECLARE THE ITEM3
public static Item MyItem_3;

//DECLARE THE FOOD
public static Item MyFood_1;

//DECLARE THE FOOD2
public static Item MyFood_2;

//DECLARE THE FOOD3
public static Item MyFood_3;

//DECLARE THE FOOD4
public static Item MyFood_4;

//DECLARE THE FOOD5
public static Item MyFood_5;

//DECLARE THE FOOD5
public static Item MyFood_6;

//DECLARE THE BLOCK
public static Block MyBlock_1;

//DECLARE THE ARMOR
public static Item MyHelmet_1;
public static Item MyChest_1;
public static Item MyLeggings_1;
public static Item MyBoots_1;

//DECLARE THE ARMOR MATERIAL
																									/** maxdamage damage thing enchant */
public static EnumArmorMaterial MyArmorMaterial_1 = EnumHelper.addArmorMaterial("HOOOOOOOOOOOOOOOOOTdog", 9000000, new int[]{902034, 920342345, 1234234452, 1234353545}, 20000000);

//DECLARE THE BIOME
public static  BiomeGenBase MyBiome_1;

//DECLARE THE MOD ID
static int MyEntityID = 300;

//  SEARCH FOR UNIQUE ID    
public static int getUniqueEntityId() {
    do {
        MyEntityID++;
    }
    while (EntityList.getStringFromID(MyEntityID) != null);
    return MyEntityID++;
}

//  DECLARE A NEW EGG
public static void registerEntityEgg(Class <? extends Entity> entity, int primaryColor, int secondaryColor) {
    int id = getUniqueEntityId();
    EntityList.IDtoClassMapping.put(id, entity);
    EntityList.entityEggs.put(id, new EntityEggInfo(id, primaryColor, secondaryColor));
    
//  DECLARE A NEW CREATIVE TAB  
    CreativeTabs MyCreativeTab_1;


}

private CreativeTabs MyCreativeTab_1;
    
/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */	


@EventHandler	
	public  void preInit( FMLPreInitializationEvent event ) 
	{
/**	
 * LOAD SECTION 
 * *********************************************************** */ 

//  LOAD THE CREATIVE TAB
    CreativeTabs MyCreativeTab_1 = new CreativeTabs("MyCreativeTab_1") {
        public ItemStack getIconItemStack() {
            return new ItemStack(MyFood_2, 1, 0);   // Icon, Stack Size, Tab Position
        }
    };
	
//  LOAD THE SWORD
    MySword_1 = new MySword(8000, LOL, "MySword_1").setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MySword_1, "MySword_1");
    LanguageRegistry.addName(MySword_1, "JUUUMBO HOTDOG SWORD");     

//  LOAD THE PICKAXE
    MyPickaxe_1 = new MyPickaxe(8001, MyToolMaterial, "MyPickaxe_1").setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyPickaxe_1, "MyPickaxe_1");
    LanguageRegistry.addName(MyPickaxe_1, "Jumbo Hotdog Pickaxe!");
    
//  LOAD THE PICKAXE2
    MyPickaxe_2 = new MyPickaxe(8002, MyToolMaterial2, "MyPickaxe_2").setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyPickaxe_2, "MyPickaxe_2");
    LanguageRegistry.addName(MyPickaxe_2, "Jumbo MUUUMBO Hotdog Pickaxe!");
    
//  LOAD THE ITEM
    MyItem_1 = new MyItem(8003, "MyItem_1").setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyItem_1, "MyItem_1");
    LanguageRegistry.addName(MyItem_1, "JUMBO HOOOOTDOG(Without the bun...)");
    
//  LOAD THE ITEM 2
    MyItem_2 = new MyItem(8004, "MyItem_2").setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyItem_2, "MyItem_2");
    LanguageRegistry.addName(MyItem_2, "Dough");
    
//  LOAD THE ITEM 3
    MyItem_3 = new MyItem(8005, "MyItem_3").setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyItem_3, "MyItem_3");
    LanguageRegistry.addName(MyItem_3, "Blocka Coala Can");
    
//  LOAD THE FOOD
    /** IID HA Cuz.. SM still Cuz.. f IWFM  */
    MyFood_1 = new MyFood(8006, 10, 2000000.0F, true, "MyFood_1").setAlwaysEdible().setPotionEffect(Potion.invisibility.id, 60, 10, 1.0F).setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyFood_1, "MyFood_1");
    LanguageRegistry.addName(MyFood_1, "JUUUUUMBO HOTDOG");
    
    
//  LOAD THE FOOD2 Y3S I SAID 2
    /** IID HA Cuz.. SM still Cuz.. f IWFM  */
    MyFood_2 = new MyFood(8007, 20000, 20000000000000.55F, true, "MyFood_2").setAlwaysEdible().setPotionEffect(Potion.waterBreathing.id, 999999999, 999999999, 9999999999999999.0F).setPotionEffect(Potion.jump.id, 999999999, 999999999, 999999999999.0F).setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyFood_2, "MyFood_2");
    LanguageRegistry.addName(MyFood_2, "JUUUUUMBO MUUMBO HOTDOG");
    
//  LOAD THE FOOD3 Y3S I SAID 3
    /** IID HA Cuz.. SM still Cuz.. f IWFM  */
    MyFood_3 = new MyFood(8008, 20000, 20000000000000.55F, true, "MyFood_3").setAlwaysEdible().setPotionEffect(Potion.waterBreathing.id, 999999999, 999999999, 9999999999999999.0F).setPotionEffect(Potion.jump.id, 999999999, 999999999, 999999999999.0F).setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyFood_3, "MyFood_3");
    LanguageRegistry.addName(MyFood_3, "Donut!");
    
//  LOAD THE FOOD4 Y3S I SAID 4
    /** IID HA Cuz.. SM still Cuz.. f IWFM  */
    MyFood_4 = new MyFood(8009, 20000, 20000000000000.55F, true, "MyFood_4").setAlwaysEdible().setPotionEffect(Potion.waterBreathing.id, 999999999, 999999999, 9999999999999999.0F).setPotionEffect(Potion.jump.id, 999999999, 999999999, 999999999999.0F).setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyFood_4, "MyFood_4");
    LanguageRegistry.addName(MyFood_4, "Icing!");
    
//  LOAD THE FOOD5 Y3S I SAID 5
    /** IID HA Cuz.. SM still Cuz.. f IWFM  */
    MyFood_5 = new MyFood(8010, 20000, 20000000000000.55F, true, "MyFood_5").setMaxStackSize(10).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyFood_5, "MyFood_5");
    LanguageRegistry.addName(MyFood_5, "Blocka Coala (not canned)");
    
//  LOAD THE BLOCK 
    MyBlock_1 = new MyBlock(8011, Material.rock, "MyBlock_1").setLightValue(1.0f).setResistance(10.0f).setHardness(112.0f).setStepSound(Block.soundGrassFootstep).setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerBlock(MyBlock_1, "MyBlock_1");
    LanguageRegistry.addName(MyBlock_1, "JUUUUUMBO HOTDOG ORE!"); 
	MinecraftForge.setBlockHarvestLevel(MyBlock_1, "pickaxe", 3);
	
//  LOAD HELMET 
    MyHelmet_1 = new MyArmor(8012, MyArmorMaterial_1, 0, 0, "myarmor").setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyHelmet_1, "MyHelmet_1");
    LanguageRegistry.addName(MyHelmet_1, "Jumbo Hotdog Helmet");      

//LOAD CHESTPLATE
    MyChest_1 = new MyArmor(8013, MyArmorMaterial_1, 0, 1, "myarmor").setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyChest_1, "MyChest_1");
    LanguageRegistry.addName(MyChest_1, "Jumbo Hotdog Chestplate");

//LOAD LEGGINGS    
    MyLeggings_1 = new MyArmor(8014, MyArmorMaterial_1, 0, 2, "myarmor").setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyLeggings_1, "MyLeggings_1");
    LanguageRegistry.addName(MyLeggings_1, "Jumbo Hotdog Leggings");

//LOAD BOOTS   
    MyBoots_1 = new MyArmor(8015, MyArmorMaterial_1, 0, 3, "myarmor").setCreativeTab(MyCreativeTab_1);
    GameRegistry.registerItem(MyBoots_1, "MyBoots_1");
    LanguageRegistry.addName(MyBoots_1, "Jumbo Hotdog Boots");
    
//  LOAD THE FOOD6 Y3S I SAID 6
    /** IID HA Cuz.. SM still Cuz.. f IWFM  */
    MyFood_6 = new MyFood(8016, 20000, 20000000000000.55F, true, "MyFood_6").setMaxStackSize(10).setCreativeTab(MyCreativeTab_1).setPotionEffect(Potion.invisibility.id, 999, 10   , 1.0F);
    GameRegistry.registerItem(MyFood_6, "MyFood_6");
    LanguageRegistry.addName(MyFood_6, "Blocka Coala");
    
//  LOAD BIOME
    MyBiome_1 = new MyBiome(30);
    GameRegistry.addBiome(MyBiome_1);
    
    
//  REGISTER YOUR ENTITY
    EntityRegistry.registerGlobalEntityID(MyEntityZombie.class, "Good Zombie", EntityRegistry.findGlobalUniqueEntityId());
    EntityRegistry.addSpawn(MyEntityZombie.class, 50, 1, 6, EnumCreatureType.creature, BiomeGenBase.swampland); 
    EntityRegistry.addSpawn(MyEntityZombie.class, 50, 1, 6, EnumCreatureType.creature, MyBiome_1);     
    registerEntityEgg(MyEntityZombie.class, (new Color(108, 227, 133)).getRGB(), (new Color(252, 0, 255)).getRGB());
    RenderingRegistry.registerEntityRenderingHandler(MyEntityZombie.class, new MyRenderZombie());
    ModLoader.addLocalization("entity.Good Zombie.name", "Good Zombie");
    
//  REGISTER YOUR ENTITY
    EntityRegistry.registerGlobalEntityID(MyEntityCreeper.class, "Baby Creeper", EntityRegistry.findGlobalUniqueEntityId());
    EntityRegistry.addSpawn(MyEntityCreeper.class, 60, 3, 5, EnumCreatureType.creature, BiomeGenBase.plains); 
    EntityRegistry.addSpawn(MyEntityCreeper.class, 100, 1, 5, EnumCreatureType.creature, MyBiome_1);     
    registerEntityEgg(MyEntityCreeper.class, (new Color(70, 179, 11)).getRGB(), (new Color(252, 0, 255)).getRGB());
    RenderingRegistry.registerEntityRenderingHandler(MyEntityCreeper.class, new MyRenderCreeper());
    ModLoader.addLocalization("entity.Baby Creeper.name", "Baby Creeper");
    
    //  REGISTER YOUR ENTITY
    EntityRegistry.registerGlobalEntityID(MyEntityMinion.class, "Dough monster", EntityRegistry.findGlobalUniqueEntityId());
    EntityRegistry.addSpawn(MyEntityMinion.class, 50, 1, 5, EnumCreatureType.creature, BiomeGenBase.plains);              
    registerEntityEgg(MyEntityMinion.class, (new Color(203, 198, 142)).getRGB(), (new Color(0, 0, 0)).getRGB());
    RenderingRegistry.registerEntityRenderingHandler(MyEntityMinion.class, new MyRenderMinion(new MyModelMinion(), 0.3F));
    ModLoader.addLocalization("entity.Dough monster.name", "Dough monster");
    


    
    

    
    
	
/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */	

	}

@EventHandler
	public static void init( FMLInitializationEvent event ) 
	{
	
/**	
 * RECIPES SECTION 
 * *********************************************************** */

//  SWORD RECIPE  
    GameRegistry.addRecipe(new ItemStack(MySword_1, 1), new Object[]
    {
            "L",
            "L",
            "X",
        
        'X', Item.bread,
        'L', MyItem_1,
    });

//  PICKAXE RECIPE  
    GameRegistry.addRecipe(new ItemStack(MyPickaxe_1, 1), new Object[]
    {
            "XXX",
            " S ",
            " S ",
        'S', Item.bread,
        'X', MyItem_1,
    });
    
//  PICKAXE RECIPE  
    GameRegistry.addRecipe(new ItemStack(MyBlock_1, 1), new Object[]
    {
            "D",
        'D', Block.dirt,
    });
    
//  PICKAXE2 RECIPE  
    GameRegistry.addRecipe(new ItemStack(MyPickaxe_2, 1), new Object[]
    {
            "XXX",
            " S ",
            " S ",
        'S', MyFood_1,
        'X', MyFood_2,
    }); 

//  ITEM RECIPE         
    GameRegistry.addRecipe(new ItemStack(MyItem_1, 1), new Object[]
    {
            " GS",
            " SG",
            "SG ",
        'S', Item.porkRaw,
        'G', Item.glowstone,
    });
    
//  ITEM2 RECIPE         
    GameRegistry.addRecipe(new ItemStack(MyItem_3, 1), new Object[]
    {
    	    "SGS",
            "SGS",
            "SSS",
        'S', Item.ingotIron,
        'G', Item.redstone,
    });
    
//  FOOD RECIPE         
    GameRegistry.addRecipe(new ItemStack(MyFood_1, 1), new Object[]
    {
            "G",
            "S",
            "G",
        'S', MyItem_1,
        'G', Item.bread
    });
    
//  FOOD2 RECIPE         
    GameRegistry.addRecipe(new ItemStack(MyFood_2, 1), new Object[]
    {
            "  S",
            " S ",
            "S  ",
        'S', MyFood_1,
    });
    
//  FOOD3 RECIPE         
    GameRegistry.addRecipe(new ItemStack(MyFood_3, 1), new Object[]
    {
            "SGS",
            "G G",
            "SGS",
        'S', Main.MyItem_2,
        'G', Main.MyFood_4,
    });
    
//  FOOD4 RECIPE         
    GameRegistry.addRecipe(new ItemStack(MyFood_4, 1), new Object[]
    {
            " S ",
            "SSS",
            " S ",
        'S', Item.sugar,
        
    });
    
//  FOOD5 RECIPE         
    GameRegistry.addRecipe(new ItemStack(MyFood_5, 2), new Object[]
    {
            "SSS",
            "SSS",
            "SSS",
        'S', Item.sugar,
        
    });
    
    
//  SMELTING RECIPE
    GameRegistry.addSmelting(MyBlock_1.blockID, (new ItemStack(MyFood_1, 7)), 45);
    
//  HELMET RECIPE   
    GameRegistry.addRecipe(new ItemStack(MyHelmet_1, 1), new Object[]
    {
            "VVV",
            "V V",
            "XXX",
        'V', Item.bread,
        'X', MyItem_1,
    });         

//  CHESTPLATE RECIPE   
    GameRegistry.addRecipe(new ItemStack(MyChest_1, 1), new Object[]
    {
            "X X",
            "XVX",
            "XVX",
        'X', Item.bread,
        'V', MyItem_1,
    });         

//  LEGGINGS RECIPE 
    GameRegistry.addRecipe(new ItemStack(MyLeggings_1, 1), new Object[]
    {
            "XXX",
            "X X",
            "X X",
        'X', Item.bread,
    });         

//  BOOTS RECIPE    
    GameRegistry.addRecipe(new ItemStack(MyBoots_1, 1), new Object[]
    {
            "V V",
            "V V",
        'V', MyItem_1,
    });             
/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */	

	
/**	
 * EXTRA METHODS SECTION 
 * ************************************************************ */

//  CHANGE TAB NAME
    LanguageRegistry.instance().addStringLocalization("itemGroup.MyCreativeTab_1", "en_US", "TEH HOTDOG TAAAAB!");   

        


/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */	

	
	}

@EventHandler
	public static void postInit( FMLPostInitializationEvent event ) 
	{

	}
	
}
